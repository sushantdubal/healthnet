Feature: Onboarding
Description: The purpose is to test Onboarding feature

# Successful Patient Login with Valid Credentials(Scenario id:1 ,Testcases covered:)
@Onboarding_Test @SmokeTest @TC0001
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Patient enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Doctor Login with Valid Credentials(Scenario id:2 ,Testcases covered:)
@Onboarding_Test @SmokeTest @TC0002
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Doctor enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful ItAdmin Login with Valid Credentials(Scenario id:3 ,Testcases covered:)
@Onboarding_Test
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Itadmin enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Itemployee Login with Valid Credentials(Scenario id:4 ,Testcases covered:)
@Onboarding_Test
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Itemployee enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Login with Valid Credentials(Scenario id:5 ,Testcases covered:)
@Onboarding_Test
Scenario: Parameterizing without Example Keyword- Successful Login with Valid Credentials 
Given User is on Home Page
When User Navigate to LogIn Page
And User enters "viren.patani@interactive.in" and "123456"
And User clicked Login button
Then Verify user logged in successfully
And Close Driver


# Successful Login with Valid Credentials(Scenario id:6 ,Testcases covered:)
@Onboarding_Test
Scenario Outline: Data Driven Testing Using Examples Keyword- Successful Login with Valid Credentials 
Given User is on Home Page
When User Navigate to LogIn Page
And User enters "<emailid>" and "<password>"
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

Examples:
    | emailid  | password |
    |  viren.patani@interactive.in | 123456 |
    | candy_096@yahoo.com | 123456 |
	|  agrawal007haresh@gmail.com | 123456 |
	| harishpatient@gmail.com| 123456 |


# Successful Login with Valid Credentials(Scenario id:7 ,Testcases covered:)
@Onboarding_Test
Scenario: Data Driven Testing Using Data Tables in cucumber- Successful Login with Valid Credentials 
Given User is on Home Page
When User Navigate to LogIn Page
And User enters Credentials to LogIn
 | viren.patani@interactive.in | 123456 |
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Login with Valid Credentials(Scenario id:8 ,Testcases covered:)
@Onboarding_Test
Scenario: Data Driven Testing Using maps in data tables- Successful Login with Valid Credentials 
Given User is on Home Page
When User Navigate to LogIn Page
And User enters Credentials to LogIn using maps
	| Emailid   | Password |
  | viren.patani@interactive.in | 123456 |
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Login with Valid Credentials(Scenario id:9 ,Testcases covered:)
@Onboarding_Test
Scenario: Data Driven Testing Using maps in data tables with multiple test data- Successful Login with Valid Credentials 
Given User is on Home Page
When User Navigate to LogIn Page
And User enters Credentials to LogIn using maps with multiple test data
	| Emailid   | Password |
  |  viren.patani@interactive.in | 123456 |
  | candy_096@yahoo.com | 123456 |
	|  agrawal007haresh@gmail.com | 123456 |
	| harishpatient@gmail.com| 123456 |
Then Verify user logged in successfully
And Close Driver


# Successful Login with Valid Credentials(Scenario id:10 ,Testcases covered:)
@Onboarding_Test
Scenario: Data Driven Testing Using maps in data tables with multiple test data- Successful Login with Valid Credentials 
Given User is on Home Page
When User Navigate to LogIn Page
And User enters Credentials to LogIn using maps with class objects
	| Emailid   | Password |
  |  viren.patani@interactive.in | 123456 |
  | candy_096@yahoo.com | 123456 |
	|  agrawal007haresh@gmail.com | 123456 |
	| harishpatient@gmail.com| 123456 |
Then Verify user logged in successfully
And Close Driver


# Successful Login with Valid Credentials(Scenario id:11 ,Testcases covered:)
@Onboarding_Test
Scenario: Data driven testing using excel- Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And User enters Emailid and Password from excel
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Unsuccessful Patient Login with invalid password(Scenario id:12 ,Testcases covered:)
@Onboarding_Test
Scenario: Unsuccessful Patient Login with invalid password
Given User is on Home Page
When User Navigate to LogIn Page
And Patient enters Emailid and invalid Password
And User clicked Login button
Then Verify user get alert message
And Close Driver

# Unsuccessful Doctor Login with invalid password(Scenario id:13 ,Testcases covered:)
@Onboarding_Test
Scenario: Unsuccessful Doctor Login with invalid password
Given User is on Home Page
When User Navigate to LogIn Page
And Doctor enters Emailid and invalid Password
And User clicked Login button
Then Verify user get alert message
And Close Driver

# Unsuccessful Itadmin Login with invalid password(Scenario id:14 ,Testcases covered:)
@Onboarding_Test
Scenario: Unsuccessful Itadmin Login with invalid password
Given User is on Home Page
When User Navigate to LogIn Page
And Itadmin enters Emailid and invalid Password
And User clicked Login button
Then Verify user get alert message
And Close Driver

# Unsuccessful Itemployee Login with invalid password(Scenario id:15 ,Testcases covered:)
@Onboarding_Test
Scenario: Unsuccessful Itemployee Login with invalid password
Given User is on Home Page
When User Navigate to LogIn Page
And Itemployee enters Emailid and invalid Password
And User clicked Login button
Then Verify user get alert message
And Close Driver

# Forgot password flow with valid email id (Scenario id:16 ,Testcases covered:)
@Onboarding_Test
Scenario: Forgot password flow with valid email id
Given User is on Home Page
When User Navigate to LogIn Page
And User clicks forgot password link
And User entered valid email id in email textbox
And Click Submit button
Then Verify alert- Please check email & change your password
And Close Driver





