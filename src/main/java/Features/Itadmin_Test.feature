Feature: Itadmin
Description: The purpose is to test Onboarding feature

# Successful Itemployee Login with Valid Credentials
@Itadmin_Test
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Itemployee enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Patient Login with Valid Credentials(Scenario id:1 ,Testcases covered:)
@Itadmin_Test
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Patient enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Doctor Login with Valid Credentials(Scenario id:2 ,Testcases covered:)
@Itadmin_Test
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Doctor enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful ItAdmin Login with Valid Credentials(Scenario id:3 ,Testcases covered:)
@Itadmin_Test
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Itadmin enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Itemployee Login with Valid Credentials(Scenario id:4 ,Testcases covered:)
@Itadmin_Test
Scenario: Successful Login with Valid Credentials
Given User is on Home Page
When User Navigate to LogIn Page
And Itemployee enters Emailid and Password
And User clicked Login button
Then Verify user logged in successfully
And Close Driver

# Successful Login with Valid Credentials(Scenario id:5 ,Testcases covered:)
@Itadmin_Test
Scenario: Parameterizing without Example Keyword- Successful Login with Valid Credentials 
Given User is on Home Page
When User Navigate to LogIn Page
And User enters "viren.patani@interactive.in" and "123456"
And User clicked Login button
Then Verify user logged in successfully
And Close Driver


