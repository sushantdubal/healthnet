package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import dataProvider.ConfigFileReader;

public class PatientPage {
	WebDriver driver;
	ConfigFileReader configFileReader;
	
	
	public PatientPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		configFileReader= new ConfigFileReader();
	}
	
	@FindBy(how = How.ID, id ="BloodGroup")
	public WebElement dropdwon_blood_group;
	
	public void user_select_blood_group() throws Throwable {
		dropdwon_blood_group.click();
		//Select oSelect = new Select(dropdwon_blood_group);
		//oSelect.selectByValue("O+");
		//System.out.println(oSelect.getAllSelectedOptions());
	}

	public void verify_selected_blood_group() throws Throwable {
	    
	}

}
