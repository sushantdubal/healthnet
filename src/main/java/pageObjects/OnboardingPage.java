package pageObjects;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.DataTable;
import dataProvider.ConfigFileReader;
import managers.FileReaderManager;
import testData.Credentials;
import utility.ExcelUtils;
import utility.ExtentReportsClass;

public class OnboardingPage  {
	WebDriver driver;
	ConfigFileReader configFileReader;
	
	
	
	public OnboardingPage(WebDriver driver) {
		this.driver= driver;
		PageFactory.initElements(driver, this);
		//configFileReader= new ConfigFileReader();
	}
	@FindBy(how = How.XPATH, using ="/html/body/app-root/mat-sidenav-container/mat-sidenav-content/div/app-header/mat-toolbar/div/mat-toolbar-row/div/div/div[3]/a[1]")
	public WebElement btn_login_homescreen;

	@FindBy(how = How.XPATH, using ="html/body/div[1]/div/div[2]/mat-dialog-container/app-login/form/div[1]/div/div[1]/input")
	public WebElement email_input_line;

	@FindBy(how = How.XPATH, using ="html/body/div[1]/div/div[2]/mat-dialog-container/app-login/form/div[1]/div/div[2]/input")
	public WebElement password_input_line;

	@FindBy(how = How.XPATH, using ="html/body/div[1]/div/div[2]/mat-dialog-container/app-login/form/div[2]/div[2]/button")
	public WebElement btn_login;
	
	@FindBy(how = How.XPATH, using ="html/body/div[1]/div/div[2]/mat-dialog-container/app-login/div/div")
	public WebElement alert_invalid_password;
	
	
	
	@FindBy(how = How.XPATH, using ="html/body/div[1]/div/div[2]/mat-dialog-container/app-login/form/div[2]/div[1]/a")
	public WebElement Link_forgot_password;

	
	@FindBy(how = How.XPATH, using ="html/body/div[1]/div/div[2]/mat-dialog-container/app-forget-password/form/div[1]/div/div/input")
	public WebElement inputbox_emailid; 
	
	
	@FindBy(how = How.XPATH, using ="html/body/div[1]/div/div[2]/mat-dialog-container/app-forget-password/form/div[2]/div/button")
	public WebElement btn_submit; 
	
	
	@FindBy(how = How.XPATH, using ="html/body/div[1]/div/div[2]/mat-dialog-container/app-forget-password/div/div")
	public WebElement alert_forgot_password;
	
	public void LoginToIHealthnet(String emailid, String password)
	{
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(btn_login_homescreen));
		btn_login_homescreen.click();
		email_input_line.sendKeys();
		password_input_line.sendKeys();
		btn_login.click();


	}
	
	
	
	public void navigateTo_HomePage() {
		driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
		
		
	}
	
	public void user_is_on_Home_Page() throws Throwable {
		//utility.TestBase.startBrowser("chrome", "http://interactive.in/clients/healthnet/");
	
	}

	public void user_Navigate_to_LogIn_Page() throws Throwable {
		btn_login_homescreen.click();
	} 

	public void patient_enters_Emailid_and_Password() throws Throwable {
		email_input_line.sendKeys("viren.patani@interactive.in");
		password_input_line.sendKeys("123456");
	}
	
	public void doctor_enters_Emailid_and_Password() throws Throwable {
		email_input_line.sendKeys("candy_096@yahoo.com");
		password_input_line.sendKeys("123456");
	}
	
	public void itadmin_enters_Emailid_and_Password() throws Throwable {
		email_input_line.sendKeys("agrawal007haresh@gmail.com");
		password_input_line.sendKeys("123456");
	}
	
	public void itemployee_enters_Emailid_and_Password() throws Throwable {
		email_input_line.sendKeys("harishpatient@gmail.com");
		password_input_line.sendKeys("123456");
	}
	
	public void user_enters_and(String emailid, String password) throws Throwable {
		email_input_line.sendKeys(emailid);
		password_input_line.sendKeys(password);
	}
	
	
	public void user_enters_Credentials_to_LogIn(DataTable usercredentials) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		//Write the code to handle Data Table
				List<List<String>> data = usercredentials.raw();
		 
				//This is to get the first data of the set (First Row + First Column)
				email_input_line.sendKeys(data.get(0).get(0)); 
		 
				//This is to get the first data of the set (First Row + Second Column)
				password_input_line.sendKeys(data.get(0).get(1));
		 
	}
	
	
	public void user_enters_Credentials_to_LogIn_using_maps(DataTable usercredentials) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		//Write the code to handle Data Table
				List<Map<String,String>> data = usercredentials.asMaps(String.class,String.class);
				email_input_line.sendKeys(data.get(0).get("Emailid")); 
				password_input_line.sendKeys(data.get(0).get("Password"));   
	}
	
	
	public void user_enters_Credentials_to_LogIn_using_maps_with_multiple_test_data(DataTable usercredentials) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		for (Map<String, String> data : usercredentials.asMaps(String.class, String.class)) {
		email_input_line.sendKeys(data.get("Emailid"));
		password_input_line.sendKeys(data.get("Password"));
		btn_login.click();
		}
	}
	
	
	public void user_enters_Credentials_to_LogIn_using_maps_with_class_objects(List<Credentials>  usercredentials) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		//Write the code to handle Data Table
			for (Credentials credentials : usercredentials) {			
				email_input_line.sendKeys(credentials.getEmailid()); 
				password_input_line.sendKeys(credentials.getPassword());
				btn_login.click();
				}		
  
	}
	
	public void user_enters_Emailid_and_Password_from_excel() throws Throwable {
		ExcelUtils.setExcelFile("/Users/healthnet/src/main/java/testData/Testdata.xlsx","Sheet1");
		 
		String sUserName = ExcelUtils.getCellData(1, 1);
		 
		String sPassword = ExcelUtils.getCellData(1, 2);
		
		System.out.println(sUserName);
		System.out.println(sPassword);
		email_input_line.sendKeys(sUserName);
		password_input_line.sendKeys(sPassword);
	}



	public void user_clicked_Login_button() throws Throwable {
		btn_login.click();
	}

	
	public void verify_user_logged_in_successfully() throws Throwable {
		
		
		String expected_text="Healthnet";
		 
		String actual_text=driver.getTitle();
		System.out.println(actual_text);
		 Assert.assertEquals(actual_text, expected_text, "Testcase fail- Assertion fails");
		 

	}
	
public void verify_user_logged_failed() throws Throwable {
		
		
		String expected_text="Healthnet1";
		 
		String actual_text=driver.getTitle();
		System.out.println(actual_text);
		 Assert.assertEquals(actual_text, expected_text, "Testcase fail- Assertion fails");
		 

	}
	public void patient_enters_Emailid_and_invalid_Password() throws Throwable {
		email_input_line.sendKeys("viren.patani@interactive.in");
		password_input_line.sendKeys("12345");
	}
	
	
	public void verify_user_get_alert_message() throws Throwable {
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		//wait.until(ExpectedConditions.elementToBeClickable(alert_invalid_password));
		Thread.sleep(20000);
		String expected_text="Invalid username and/or password";
		//String actual_text= driver.getTitle();
		String actual_text= alert_invalid_password.getText();
		System.out.println(actual_text);
		Assert.assertEquals(actual_text, expected_text, "Testcase fail- Assertion fails");
		
		
	}
	
	public void doctor_enters_Emailid_and_invalid_Password() throws Throwable {
		email_input_line.sendKeys("viren.patani@interactive.in");
		password_input_line.sendKeys("12345");
	}
	
	public void itadmin_enters_Emailid_and_invalid_Password() throws Throwable {
		email_input_line.sendKeys("viren.patani@interactive.in");
		password_input_line.sendKeys("12345");
	}
	
	
	public void itemployee_enters_Emailid_and_invalid_Password() throws Throwable {
		email_input_line.sendKeys("viren.patani@interactive.in");
		password_input_line.sendKeys("12345");
	}

	
	public void user_clicks_forgot_password_link() throws Throwable {
	   Link_forgot_password.click();
	}

	
	public void user_entered_valid_email_id_in_email_textbox() throws Throwable {
	    inputbox_emailid.sendKeys("harishpatient@gmail.com");
	}

	
	public void click_Submit_button() throws Throwable {
	  btn_submit.click();
	}

	
	public void verify_alert_Please_check_email_change_your_password() throws Throwable {
		Thread.sleep(30000);
		String expected_text="Please check email and change your password";
		//String actual_text= driver.getTitle();
		String actual_text= alert_forgot_password.getText();
		System.out.println(actual_text);
		Assert.assertEquals(actual_text, expected_text, "Testcase fail- Assertion fails");
	}
	
	public void close()
	{
		driver.close();
	}
	
	
	public void quit()
	{
		driver.quit();
	}
	
	
	


public void user_enters_emailid_and_password_from_dataprovider(String emailid, String password) throws Throwable {
	email_input_line.sendKeys(emailid);
	password_input_line.sendKeys(password);
}

	
}
	
	
	
	
	
	
	
	
	

