package managers;
 
 
 
import org.openqa.selenium.WebDriver;

import pageObjects.OnboardingPage;
import pageObjects.PatientPage;
 
//import pageObjects.CheckoutPage;
 
//import pageObjects.ConfirmationPage;
 
//import pageObjects.HomePage;
 
//import pageObjects.ProductListingPage;
 
 
 
public class PageObjectManager {
 
	private WebDriver driver;
	
	private OnboardingPage onboardingPage;
 
	private PatientPage patientPage;
 
	//private CartPage cartPage;
 
	//private HomePage homePage;
 
	//private CheckoutPage checkoutPage;
 
	//private ConfirmationPage confirmationPage;
 
	
 
	public PageObjectManager(WebDriver driver) {
 
		this.driver = driver;
 
	}
 
	
 
	public OnboardingPage getOnboardingPage(){
 
		return (onboardingPage == null) ? onboardingPage = new OnboardingPage(driver) : onboardingPage;
 
	}
 
	
 
	public PatientPage getPatientPage() {
 
		return (patientPage == null) ? patientPage = new PatientPage(driver) : patientPage;
 
	}
 
	
 
	/*public CartPage getCartPage() {
 
		return (cartPage == null) ? cartPage = new CartPage(driver) : cartPage;
 
	}
 
	
 
	public CheckoutPage getCheckoutPage() {
 
		return (checkoutPage == null) ? checkoutPage = new CheckoutPage(driver) : checkoutPage;
 
	}*/
}