/**
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import utility.TestBase;

public class LoginPage extends TestBase {
	WebDriver driver;
	
	public void LoginPageNew(WebDriver ldriver)
	{
		this.driver=ldriver;
	}
	
	@FindBy(how=How.XPATH, using="html/body/app-root/mat-sidenav-container/mat-sidenav-content/div/app-header/mat-toolbar/div/mat-toolbar-row/div/div/div[2]/a[1]")
	@CacheLookup
	WebElement btn_Login_Homescreen;
	
	@FindBy(id="userEmailId")
	@CacheLookup
	WebElement emailid_input_box;
	
	@FindBy(how=How.ID, using="password")
	@CacheLookup
	WebElement password_input_box;
	
	@FindBy(how=How.XPATH, using="html/body/div[1]/div/div[2]/mat-dialog-container/app-login/form/div[2]/div[2]/button")
	@CacheLookup
	WebElement btn_Login;
	
	@FindBy(how=How.LINK_TEXT, using="Forgot Password")
	@CacheLookup
	WebElement link_forgot_password;
	
	public void login_iLoveLondon(String email_id, String password)
	{
	btn_Login_Homescreen.click();
	emailid_input_box.sendKeys(email_id);
	password_input_box.sendKeys(password);
	btn_Login.click();
	}
	

}
