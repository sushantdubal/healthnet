/**
 * 
 */
package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import pages.OnboardingPage;
import utility.BrowserFactory;
import utility.Constant;
import utility.ExcelUtils;


/** 
 * @author Sushant
 * Jul 24, 2018
 * 7:22:29 PM
 */
public class VerifyValidLoginAsDoctor {
	WebDriver driver;
	@Test
	public void checkValidUser()throws Throwable
	{
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		 
		String sUserName = ExcelUtils.getCellData(2, 1);
		 
		String sPassword = ExcelUtils.getCellData(2, 2);
		
		System.out.println(sUserName);
		System.out.println(sPassword);
		
		//This will launch browser and specific url
		driver=BrowserFactory.startBrowser("chrome","http://interactive.in/clients/healthnet");
		
		//created page object using page factory
		OnboardingPage onboarding_page=PageFactory.initElements(driver, OnboardingPage.class);
		
		//LoginPage login_page_old=PageFactory.initElements(driver, LoginPage.class);
		
		//call method
		onboarding_page.LoginToHealthnetAsDoctor(sUserName, sPassword);
		
		
	}
	
	@AfterTest
	public void quit() 
	{
		driver.quit();
	}
}


