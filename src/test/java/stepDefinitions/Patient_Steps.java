package stepDefinitions;

import managers.PageObjectManager;
import managers.WebDriverManager;

import org.openqa.selenium.WebDriver;

import pageObjects.OnboardingPage;
import pageObjects.PatientPage;
import cucumber.TestContext;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dataProvider.ConfigFileReader;

public class Patient_Steps {
	
	WebDriver driver;
	OnboardingPage onboardingPage;
	PageObjectManager pageObjectManager;
	ConfigFileReader configFileReader;
	WebDriverManager webDriverManager;
	PatientPage patientPage;
	TestContext testContext;
	public Patient_Steps(TestContext context) {
		testContext = context;
		patientPage = testContext.getPageObjectManager().getPatientPage();
	}
	
	@When("^User select blood group$")
	public void user_select_blood_group() throws Throwable {
		patientPage.user_select_blood_group();
	}

	@Then("^Verify selected blood group$")
	public void verify_selected_blood_group() throws Throwable {
		patientPage.verify_selected_blood_group();
	}

}
