package stepDefinitions;

import java.util.List;

import managers.FileReaderManager;
import managers.PageObjectManager;
import managers.WebDriverManager;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;

//import Data_driven_using_excel.PendingException;
import pageObjects.OnboardingPage;
import pageObjects.PatientPage;
import testData.Credentials;
import cucumber.TestContext;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dataProvider.ConfigFileReader;

public class Onboarding_Steps {
	OnboardingPage onboardingPage;
	TestContext testContext;
	public Onboarding_Steps(TestContext context) {
		testContext = context;
		onboardingPage = testContext.getPageObjectManager().getOnboardingPage();
	}
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
		onboardingPage.navigateTo_HomePage();
	}

	@When("^User Navigate to LogIn Page$")
	public void user_Navigate_to_LogIn_Page() throws Throwable {
		onboardingPage.user_Navigate_to_LogIn_Page();
	}

	@When("^Patient enters Emailid and Password$")
	public void patient_enters_Emailid_and_Password() throws Throwable {
		onboardingPage.patient_enters_Emailid_and_Password();
	}
	
	@When("^Doctor enters Emailid and Password$")
	public void doctor_enters_Emailid_and_Password() throws Throwable {
		onboardingPage.doctor_enters_Emailid_and_Password();
	}
	
	@When("^Itadmin enters Emailid and Password$")
	public void itadmin_enters_Emailid_and_Password() throws Throwable {
		onboardingPage.itadmin_enters_Emailid_and_Password();
	}
	
	@When("^Itemployee enters Emailid and Password$")
	public void itemployee_enters_Emailid_and_Password() throws Throwable {
		onboardingPage.itemployee_enters_Emailid_and_Password();
	}
	
	@When("^User enters \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_and(String emailid, String password) throws Throwable {
		onboardingPage.user_enters_and(emailid, password);
	}
	@When("^User enters Credentials to LogIn$")
	public void user_enters_Credentials_to_LogIn(DataTable usercredentials) throws Throwable {
		onboardingPage.user_enters_Credentials_to_LogIn(usercredentials);
	}
	
	@When("^User enters Credentials to LogIn using maps$")
	public void user_enters_Credentials_to_LogIn_using_maps(DataTable usercredentials) throws Throwable {
		onboardingPage.user_enters_Credentials_to_LogIn_using_maps(usercredentials);
	}

	@When("^User enters Credentials to LogIn using maps with multiple test data$")
	public void user_enters_Credentials_to_LogIn_using_maps_with_multiple_test_data(DataTable usercredentials) throws Throwable {
		onboardingPage.user_enters_Credentials_to_LogIn_using_maps_with_multiple_test_data(usercredentials);
	}
	
	@When("^User enters Credentials to LogIn using maps with class objects$")
	public void user_enters_Credentials_to_LogIn_using_maps_with_class_objects(List<Credentials>  usercredentials) throws Throwable {
		onboardingPage.user_enters_Credentials_to_LogIn_using_maps_with_class_objects(usercredentials);
	}
	
	@When("^User enters Emailid and Password from excel$")
	public void user_enters_Emailid_and_Password_from_excel() throws Throwable {
		onboardingPage.user_enters_Emailid_and_Password_from_excel();
	}

	@When("^User clicked Login button$")
	public void user_clicked_Login_button() throws Throwable {
		onboardingPage.user_clicked_Login_button();
	}
	
	
	@Then("^Verify user logged in successfully$")
	public void verify_user_logged_in_successfully() throws Throwable {
		onboardingPage.verify_user_logged_in_successfully();	
	}
	
	@When("^Patient enters Emailid and invalid Password$")
	public void patient_enters_Emailid_and_invalid_Password() throws Throwable {
		onboardingPage.patient_enters_Emailid_and_invalid_Password();
	}
	
	@Then("^Verify user get alert message$")
	public void verify_user_get_alert_message() throws Throwable {
		onboardingPage.verify_user_get_alert_message();
	}
	
	@When("^Doctor enters Emailid and invalid Password$")
	public void doctor_enters_Emailid_and_invalid_Password() throws Throwable {
		onboardingPage.doctor_enters_Emailid_and_invalid_Password();
	}
	
	@When("^Itadmin enters Emailid and invalid Password$")
	public void itadmin_enters_Emailid_and_invalid_Password() throws Throwable {
		onboardingPage.itadmin_enters_Emailid_and_invalid_Password();
	}
	
	@When("^Itemployee enters Emailid and invalid Password$")
	public void itemployee_enters_Emailid_and_invalid_Password() throws Throwable {
		onboardingPage.itemployee_enters_Emailid_and_invalid_Password();
	}
	
	@When("^User clicks forgot password link$")
	public void user_clicks_forgot_password_link() throws Throwable {
	   onboardingPage.user_clicks_forgot_password_link();
	}

	@When("^User entered valid email id in email textbox$")
	public void user_entered_valid_email_id_in_email_textbox() throws Throwable {
		onboardingPage.user_entered_valid_email_id_in_email_textbox();
	}

	@When("^Click Submit button$")
	public void click_Submit_button() throws Throwable {
		onboardingPage.click_Submit_button();
	}
	
	@When("^Quit Driver$")
	public void quit()
	{
		onboardingPage.quit();
	}
		
	@When("^Close Driver$")
	public void close()
	{
		onboardingPage.close();
	}

	@Then("^Verify alert- Please check email & change your password$")
	public void verify_alert_Please_check_email_change_your_password() throws Throwable {
		onboardingPage.verify_alert_Please_check_email_change_your_password();
	}
	
}
