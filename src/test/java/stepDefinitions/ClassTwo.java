package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
@Test
public class ClassTwo {
	WebDriver driver;
 String Actualtext;
 @BeforeClass
 public void load_url(){
	 System.setProperty("webdriver.chrome.driver", "F:/Software Testing/2. Automation Testing/QA Tools/General/chromedriver_win32r/chromedriver.exe");
		driver = new ChromeDriver();
  driver.navigate().to("http://only-testing-blog.blogspot.in/2014/01/textbox.html");
 } 
 //Method Example For Assertion
 
  public void assertion_method_1() {
  Actualtext = driver.findElement(By.xpath("//h2/span")).getText();
  Assert.assertEquals(Actualtext, "Tuesday, 28 January 2014");
  System.out.print("\n assertion_method_1() -> Part executed");
 } 
 //Method Example For Assertion
 @Test
 public void assertion_method_2() {  
  Assert.assertEquals(Actualtext, "Tuesday, 29 January 2014");
  System.out.print("\n assertion_method_2() -> Part executed");
 }
 
 //Method Example For Verification
 @Test
 public void verification_method() {
  
  String time = driver.findElement(By.xpath("//div[@id='timeLeft']")).getText();
  
  if (time == "Tuesday, 28 January 2014")
  {
   System.out.print("\nText Match");
  }
  else
  {
   System.out.print("\nText does Match");
  }
 }
}